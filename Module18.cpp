#include <iostream>
using namespace std;

struct DataStack
{
	DataStack* next;
	int data;
};

class Stack
{
private:
	DataStack* top = nullptr;

public:
	
	void Push(int val)
	{
		DataStack* current_elem = new DataStack;
		current_elem->data = val;
		current_elem->next = top;
		top = current_elem;
	}

	void Pop()
	{
		int poptop = top->data;
		DataStack* current_elem = top;
		top = top->next;
		delete(current_elem);
		cout << poptop << '\n';
	}

	void Scroll()
	{
		DataStack* current_elem = top;
		while (current_elem != nullptr)
		{
			cout << current_elem->data << ' ';
			current_elem = current_elem->next;
		}
		cout << '\n';
	}
};


int main()
{
	Stack s1;
	s1.Push(5);
	s1.Push(7);
	s1.Push(9);
	s1.Push(11);
	s1.Scroll(); 
	s1.Pop();
	s1.Scroll();
	s1.Pop();
	s1.Scroll();
	s1.Pop();
	s1.Scroll();
	s1.Pop();
	s1.Scroll();
	
	
}

